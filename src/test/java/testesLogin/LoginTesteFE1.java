package testesLogin;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import junit.framework.Assert;
import pages.LoginPages;

public class LoginTesteFE1 {
	
	static WebDriver driver;
	static LoginPages loginPages;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		System.setProperty("webdriver.chrome.driver", "D:/eclipse/chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("start-maximized");
		driver = new ChromeDriver();
		
		driver.get("http://provaqa.marketpay.com.br:9080/desafioqa/login");
		loginPages = new LoginPages(driver);
		
	}

	
	@Test
	public void test() throws InterruptedException {
		loginPages.PreencherCamposFE1();
		Assert.assertEquals(loginPages.ValidarCredInvalidas(), "Credenciais inválidas");
		Thread.sleep(2000);
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		driver.quit();
	}

}
