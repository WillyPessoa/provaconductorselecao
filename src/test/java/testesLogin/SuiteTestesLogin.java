package testesLogin;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ LoginTesteFA.class, LoginTesteFE1.class, LoginTesteFP.class })
public class SuiteTestesLogin {

}
