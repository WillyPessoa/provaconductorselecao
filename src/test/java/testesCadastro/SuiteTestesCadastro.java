package testesCadastro;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CadastroEditarFP.class, CadastroExcluirFP.class, CadastroIncluirFA.class, CadastroIncluirFE1.class,
		CadastroIncluirFE2.class, CadastroIncluirFP.class, CadastroListarFA.class, CadastroListarFE1.class,
		CadastroListarFE2.class, CadastroListarFP.class })
public class SuiteTestesCadastro {

}
