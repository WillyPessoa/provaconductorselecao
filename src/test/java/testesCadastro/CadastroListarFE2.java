package testesCadastro;

import java.sql.Driver;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import junit.framework.Assert;
import pages.CadastroPages;
import pages.LoginPages;

public class CadastroListarFE2 {

	static WebDriver driver;
	static CadastroPages cadastroPages;
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		System.setProperty("webdriver.chrome.driver", "D:/eclipse/chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("start-maximized");
		driver = new ChromeDriver();

		driver.get("http://provaqa.marketpay.com.br:9080/desafioqa/login");
		driver.manage().window().maximize();
		cadastroPages = new CadastroPages(driver);

	}

	@Test
	public void paginaCadastroListarFE2() throws InterruptedException {
		cadastroPages.FormularioCadastroListar();
		Assert.assertEquals(cadastroPages.PaginaCadastroValidar(), "Listar Clientes");
		cadastroPages.FormularioCadastroListarFE2();
		Assert.assertEquals(cadastroPages.PaginaCadastroValidarDataVld(), "2019-05-17");
		Thread.sleep(2000);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		driver.quit();
	}

}