package pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPages {

	static WebDriver driver;

	public LoginPages(WebDriver driver) {
		LoginPages.driver = driver;
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	public void PreencherCamposFP() {

		WebElement nome = driver.findElement(By.name("username"));
		nome.sendKeys("admin");

		WebElement senha = driver.findElement(By.name("password"));
		senha.sendKeys("admin");

		WebElement botaoLogar = driver.findElement(By.className("btn"));
		botaoLogar.click();
		
	}
	
	public void PreencherCamposFA() {

		WebElement nome = driver.findElement(By.name("username"));
		nome.sendKeys("admin");

		WebElement senha = driver.findElement(By.name("password"));
		senha.sendKeys("admin");

		WebElement botaoLembrar = driver.findElement(By.className("checkbox"));
		botaoLembrar.click();
		
	}
	
	public void PreencherCamposFE1() {

		WebElement nome = driver.findElement(By.name("username"));
		nome.sendKeys("admin");

		WebElement senha = driver.findElement(By.name("password"));
		senha.sendKeys("SenhaBug");

		WebElement botaoLogar = driver.findElement(By.className("btn"));
		botaoLogar.click();
	}

	public String ValidarLogin() {
		
		return driver.findElement(By.className("page-title")).getText();

	}
	
public String ValidarCredInvalidas() {
		
		return driver.findElement(By.xpath("//*[@id=\"login-form\"]/fieldset/section[1]/font/label")).getText();

	}
}
