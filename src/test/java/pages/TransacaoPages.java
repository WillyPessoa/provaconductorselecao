package pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class TransacaoPages {

	static WebDriver driver;

	public TransacaoPages(WebDriver driver) {
		TransacaoPages.driver = driver;
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	// PAGE OBJECTS PARA P�GINA DE INCLUIR TRANSACOES

	public void FormularioTransacaoIncluir() {

		WebElement nome = driver.findElement(By.name("username"));
		nome.sendKeys("admin");

		WebElement senha = driver.findElement(By.name("password"));
		senha.sendKeys("admin");

		WebElement botaoLogar = driver.findElement(By.className("btn"));
		botaoLogar.click();

		WebElement hoverCoracao = driver.findElement(By.xpath("//*[@id=\"left-panel\"]/nav/ul[2]/li/a/i"));
		hoverCoracao.click();

		WebElement hoverTransacao = driver
				.findElement(By.xpath("//aside[@id='left-panel']/nav/ul[2]/li/ul/li[2]/a/span"));
		hoverTransacao.click();

		WebElement incluirTransacao = driver
				.findElement(By.xpath("//aside[@id='left-panel']/nav/ul[2]/li/ul/li[2]/ul/li/a/span"));
		incluirTransacao.click();

	}

	public void FormularioTransacaoFP() throws InterruptedException {

		Select status = new Select(driver.findElement(By.id("cliente")));
		status.selectByVisibleText("Willy Costa Lima Pessoa");

		WebElement senhaLimpar = driver.findElement(By.id("valorTransacao"));
		senhaLimpar.clear();

		WebElement senha = driver.findElement(By.id("valorTransacao"));
		senha.sendKeys("500,00");

		WebElement hoverTransacao = driver.findElement(By.id("botaoSalvar"));
		hoverTransacao.click();

		Thread.sleep(5000);

	}

	public String PaginaTransacaoValidar() {

		return driver.findElement(By.className("page-title")).getText();

	}

	public String PaginaVisualizarTransacaoValidar() {

		return driver.findElement(By.className("page-title")).getText();

	}

	// PAGE OBJECTS PARA P�GINA DE LISTAR TRANSACOES

	public void FormularioTransacaoListar() {

		WebElement nome = driver.findElement(By.name("username"));
		nome.sendKeys("admin");

		WebElement senha = driver.findElement(By.name("password"));
		senha.sendKeys("admin");

		WebElement botaoLogar = driver.findElement(By.className("btn"));
		botaoLogar.click();

		WebElement hoverCoracao = driver.findElement(By.xpath("//*[@id=\"left-panel\"]/nav/ul[2]/li/a/i"));
		hoverCoracao.click();

		WebElement hoverTransacao = driver
				.findElement(By.xpath("//aside[@id='left-panel']/nav/ul[2]/li/ul/li[2]/a/span"));
		hoverTransacao.click();

		WebElement listarTransacao = driver
				.findElement(By.xpath("//*[@id=\"left-panel\"]/nav/ul[2]/li/ul/li[2]/ul/li[2]/a"));
		listarTransacao.click();

	}

	public void FormularioTransacaoListarFP() {

		WebElement botaoPesquisar = driver.findElement(By.name("j_idt20"));
		botaoPesquisar.click();

	}
	
	public void FormularioTransacaoListarFA() {

		WebElement botaoPesquisar = driver.findElement(By.name("j_idt20"));
		botaoPesquisar.click();

		WebElement botaoDetalhesTransacao = driver.findElement(By.xpath("//*[@id=\"formListarTransacao\"]/div/div/div/table/tbody/tr[1]/td[5]/a"));
		botaoDetalhesTransacao.click();
	}

	public String PaginaTransacaoListarValidar() {

		return driver.findElement(By.className("page-title")).getText();

	}
	
	public String PaginaTransacaoDetalhesValidar() {

		return driver.findElement(By.className("page-title")).getText();

	}
	
	public String PaginaTransacaoListarValidarNome() {

		return driver.findElement(By.xpath("//*[@id=\"formListarTransacao\"]/div/div/div/table/tbody/tr[1]/td[1]")).getText();

	}
}