package pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class CadastroPages {

	static WebDriver driver;

	public CadastroPages(WebDriver driver) {
		CadastroPages.driver = driver;
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	// PAGE OBJECTS PARA P�GINA DE INCLUIR CADASTROS
	
	public void FormularioCadastro() {

		WebElement nome = driver.findElement(By.name("username"));
		nome.sendKeys("admin");

		WebElement senha = driver.findElement(By.name("password"));
		senha.sendKeys("admin");

		WebElement botaoLogar = driver.findElement(By.className("btn"));
		botaoLogar.click();

		WebElement HoverCoracao = driver.findElement(By.xpath("//*[@id=\"left-panel\"]/nav/ul[2]/li/a/i"));
		HoverCoracao.click();

		WebElement HoverCliente = driver.findElement(By.xpath("//*[@id=\"left-panel\"]/nav/ul[2]/li/ul/li[1]/a/span"));
		HoverCliente.click();

		WebElement incluirCadastro = driver.findElement(By.linkText("Incluir"));
		incluirCadastro.click();

	}

	public void FormularioCadastroFP() throws InterruptedException {

		WebElement nomeUsuario = driver.findElement(By.id("nome"));
		nomeUsuario.sendKeys("Willy Costa Lima Pessoa");

		WebElement cpfapagar = driver.findElement(By.name("cpf"));
		cpfapagar.sendKeys("");

		WebElement cpf = driver.findElement(By.name("cpf"));
		cpf.sendKeys("10238621405");

		Select status = new Select(driver.findElement(By.id("status")));
		status.selectByVisibleText("Ativo");

		WebElement saldo = driver.findElement(By.id("saldoCliente"));
		saldo.sendKeys("500,00");

		WebElement btSalvar = driver.findElement(By.id("botaoSalvar"));
		btSalvar.click();

	}

	public void FormularioCadastroFA() throws InterruptedException {

		WebElement nomeUsuario = driver.findElement(By.id("nome"));
		nomeUsuario.sendKeys("Willy Costa Lima Pessoa");

		WebElement cpfapagar = driver.findElement(By.name("cpf"));
		cpfapagar.sendKeys("");

		WebElement cpf = driver.findElement(By.name("cpf"));
		cpf.sendKeys("10238621405");

		Select status = new Select(driver.findElement(By.id("status")));
		status.selectByVisibleText("Inativo");

		WebElement saldo = driver.findElement(By.id("saldoCliente"));
		saldo.sendKeys("500,00");

		WebElement btSalvar = driver.findElement(By.id("botaoSalvar"));
		btSalvar.click();

	}

	public void FormularioCadastroFE1() throws InterruptedException {

		WebElement cpfapagar = driver.findElement(By.name("cpf"));
		cpfapagar.sendKeys("");

		WebElement cpf = driver.findElement(By.name("cpf"));
		cpf.sendKeys("10238621405");

		Select status = new Select(driver.findElement(By.id("status")));
		status.selectByVisibleText("Ativo");

		WebElement btSalvar = driver.findElement(By.id("botaoSalvar"));
		btSalvar.click();

	}

	public void FormularioCadastroFE2() throws InterruptedException {

		WebElement nomeUsuario = driver.findElement(By.id("nome"));
		nomeUsuario.sendKeys("Willy Costa Lima Pessoa");

		Select status = new Select(driver.findElement(By.id("status")));
		status.selectByVisibleText("Ativo");

		WebElement btSalvar = driver.findElement(By.id("botaoSalvar"));
		btSalvar.click();

	}

	public String PaginaCadastroValidar() {

		return driver.findElement(By.className("page-title")).getText();

	}

	public String PaginaCadastroValidarMSGNome() {

		return driver
				.findElement(By.xpath("//*[@id=\"formIncluirAlterarCliente\"]/div/div/fieldset[1]/div/div/div/small"))
				.getText();

	}

	public String PaginaCadastroValidarMSGCPF() {

		return driver
				.findElement(By.xpath("//*[@id=\"formIncluirAlterarCliente\"]/div/div/fieldset[2]/div/div/div/small"))
				.getText();
	}
	// PAGE OBJECTS PARA P�GINA DE LISTAR CADASTROS

	public void FormularioCadastroListar() {

		WebElement nome = driver.findElement(By.name("username"));
		nome.sendKeys("admin");

		WebElement senha = driver.findElement(By.name("password"));
		senha.sendKeys("admin");

		WebElement botaoLogar = driver.findElement(By.className("btn"));
		botaoLogar.click();

		WebElement HoverCoracao = driver.findElement(By.xpath("//*[@id=\"left-panel\"]/nav/ul[2]/li/a/i"));
		HoverCoracao.click();

		WebElement HoverCliente = driver.findElement(By.xpath("//*[@id=\"left-panel\"]/nav/ul[2]/li/ul/li[1]/a/span"));
		HoverCliente.click();

		WebElement listarCadastro = driver.findElement(By.linkText("Listar"));
		listarCadastro.click();

	}

	public void FormularioCadastroListarFP() throws InterruptedException {

		WebElement buscarUsuario = driver.findElement(By.name("j_idt17"));
		buscarUsuario.sendKeys("Willy Pessoa");

		WebElement botaoPesquisar = driver.findElement(By.name("j_idt20"));
		botaoPesquisar.click();

	}

	public void FormularioCadastroListarFA() throws InterruptedException {

		WebElement calendarioLimpar = driver.findElement(By.id("calendario_input"));
		calendarioLimpar.sendKeys("");

		WebElement calendarioData = driver.findElement(By.id("calendario_input"));
		calendarioData.sendKeys("05/2018");

		WebElement botaoPesquisar = driver.findElement(By.name("j_idt20"));
		botaoPesquisar.click();

	}

	public void FormularioCadastroListarFE1() throws InterruptedException {

		WebElement clienteBloqueado = driver
				.findElement(By.xpath("//form[@id='formListarCliente']/div/div/div[2]/div/span/i"));
		clienteBloqueado.click();

		WebElement clienteAtivo = driver
				.findElement(By.xpath("//form[@id='formListarCliente']/div/div/div/div/span/i"));
		clienteAtivo.click();

		WebElement botaoPesquisar = driver.findElement(By.name("j_idt20"));
		botaoPesquisar.click();

	}
	
	public void FormularioCadastroListarFE2() throws InterruptedException {

		WebElement calendarioLimpar = driver.findElement(By.id("calendario_input"));
		calendarioLimpar.sendKeys("");

		WebElement calendarioData = driver.findElement(By.id("calendario_input"));
		calendarioData.sendKeys("25/0001");

		WebElement botaoPesquisar = driver.findElement(By.name("j_idt20"));
		botaoPesquisar.click();

	}

	public String PaginaCadastroValidarListar() {

		return driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/h1")).getText();
	}

	public String PaginaCadastroValidarNome() {

		return driver.findElement(By.xpath("//*[@id=\"formListarCliente\"]/div/div/table/tbody/tr/td[1]")).getText();
	}
	
	public String PaginaCadastroValidarDataVld() {

		return driver.findElement(By.xpath("//*[@id=\"formListarCliente\"]/div/div/table/tbody/tr[1]/td[4]")).getText();
	}

	
	// PAGE OBJECTS PARA P�GINA DE EDITAR CADASTROS
	
	public void FormularioCadastroEditar() {

		WebElement nome = driver.findElement(By.name("username"));
		nome.sendKeys("admin");

		WebElement senha = driver.findElement(By.name("password"));
		senha.sendKeys("admin");

		WebElement botaoLogar = driver.findElement(By.className("btn"));
		botaoLogar.click();

		WebElement HoverCoracao = driver.findElement(By.xpath("//*[@id=\"left-panel\"]/nav/ul[2]/li/a/i"));
		HoverCoracao.click();

		WebElement HoverCliente = driver.findElement(By.xpath("//*[@id=\"left-panel\"]/nav/ul[2]/li/ul/li[1]/a/span"));
		HoverCliente.click();

		WebElement listarCadastro = driver.findElement(By.linkText("Listar"));
		listarCadastro.click();
		
		WebElement botaoPesquisar = driver.findElement(By.name("j_idt20"));
		botaoPesquisar.click();
		
		WebElement detalhesCadstro = driver.findElement(By.xpath("//*[@id=\"formListarCliente\"]/div/div/table/tbody/tr[1]/td[5]/a[1]"));
		detalhesCadstro.click();
		
		WebElement botaoAlterar = driver.findElement(By.linkText("Alterar"));
		botaoAlterar.click();

	}
	
	public void FormularioEditarFP() throws InterruptedException {

		WebElement limparNome = driver.findElement(By.id("nome"));
		limparNome.clear();
		
		WebElement nomeUsuario = driver.findElement(By.id("nome"));
		nomeUsuario.sendKeys("Willy Editado");

		WebElement cpfapagar = driver.findElement(By.name("cpf"));
		cpfapagar.sendKeys("");

		WebElement cpf = driver.findElement(By.name("cpf"));
		cpf.sendKeys("10238621405");

		Select status = new Select(driver.findElement(By.id("status")));
		status.selectByVisibleText("Ativo");

		WebElement saldoLimpar = driver.findElement(By.id("saldoCliente"));
		saldoLimpar.clear();;
		
		WebElement saldo = driver.findElement(By.id("saldoCliente"));
		saldo.sendKeys("500,00");

		WebElement btSalvar = driver.findElement(By.id("botaoSalvar"));
		btSalvar.click();

	}
	
	// PAGE OBJECTS PARA P�GINA DE EXCLUIR CADASTROS
	
	public void FormularioCadastroExcluir() {

		WebElement nome = driver.findElement(By.name("username"));
		nome.sendKeys("admin");

		WebElement senha = driver.findElement(By.name("password"));
		senha.sendKeys("admin");

		WebElement botaoLogar = driver.findElement(By.className("btn"));
		botaoLogar.click();

		WebElement HoverCoracao = driver.findElement(By.xpath("//*[@id=\"left-panel\"]/nav/ul[2]/li/a/i"));
		HoverCoracao.click();

		WebElement HoverCliente = driver.findElement(By.xpath("//*[@id=\"left-panel\"]/nav/ul[2]/li/ul/li[1]/a/span"));
		HoverCliente.click();

		WebElement listarCadastro = driver.findElement(By.linkText("Listar"));
		listarCadastro.click();
		
		WebElement botaoPesquisar = driver.findElement(By.name("j_idt20"));
		botaoPesquisar.click();
		
		WebElement excluirCadastro = driver.findElement(By.xpath("//*[@id=\"formListarCliente\"]/div/div/table/tbody/tr[1]/td[5]/a[2]"));
		excluirCadastro.click();

	}

}