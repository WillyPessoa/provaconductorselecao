package testesTransacao;

import java.sql.Driver;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import junit.framework.Assert;
import pages.CadastroPages;
import pages.LoginPages;
import pages.TransacaoPages;

public class TransacaoListarFP {

	static WebDriver driver;
	static TransacaoPages transacaoPages;
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		System.setProperty("webdriver.chrome.driver", "D:/eclipse/chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("start-maximized");
		driver = new ChromeDriver();

		driver.get("http://provaqa.marketpay.com.br:9080/desafioqa/login");
		driver.manage().window().maximize();
		transacaoPages = new TransacaoPages(driver);

	}

	@Test
	public void paginaListarFP() throws InterruptedException {
		transacaoPages.FormularioTransacaoListar();
		Assert.assertEquals(transacaoPages.PaginaTransacaoListarValidar(), "Listar Transações");
		transacaoPages.FormularioTransacaoListarFP();
		Assert.assertEquals(transacaoPages.PaginaTransacaoListarValidarNome(), "Willy Costa Lima Pessoa");
		Thread.sleep(2000);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		driver.quit();
	}

}