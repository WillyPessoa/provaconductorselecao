package testesTransacao;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TransacaoIncluirFP.class, TransacaoListarFA.class, TransacaoListarFP.class })
public class SuiteTestesTransacao {

}
